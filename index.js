const input = document.querySelector('.input');
const form = document.querySelector('.form')
let seconds = 0;
input.addEventListener ('input', (e) => {
        seconds = e.target.value;
}) 
form.addEventListener('submit', (e) => {
    e.preventDefault();
    seconds = seconds.replace( /[^0-9]/g, '');
    seconds = parseInt(seconds);

    document.documentElement.style.setProperty('--time-anim', `${seconds}s`);
})